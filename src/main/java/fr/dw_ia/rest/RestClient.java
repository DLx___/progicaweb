package fr.dw_ia.rest;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;


@ManagedBean
@SessionScoped
public class RestClient implements Serializable
{

    /**
     * @author IOOSSEN AURELIEN created on 07/11/2019
     */
    private WebTarget webtarget;
    private String ADRESSE_URI_REST_SERVICE;


    public RestClient()
    {
    }


    @PostConstruct
    protected void initialize()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ADRESSE_URI_REST_SERVICE = facesContext.getExternalContext().getInitParameter("URI_PARAM_REST");
        webtarget = (WebTarget) ClientBuilder.newClient().target(ADRESSE_URI_REST_SERVICE);


    }

    public Response get(String path)
    {

        Invocation.Builder invoc = webtarget.path(path).request(MediaType.APPLICATION_JSON);
        return invoc.get();
    }

}
