package fr.dw_ia.dao;

import fr.dw_ia.job.Departement;
import fr.dw_ia.job.Region;
import fr.dw_ia.job.Ville;
import java.sql.*;
import java.util.ArrayList;

public class VilleDAO extends DAO
{

    /**
     * @author IOOSSEN AURELIEN created on 28/08/2019
     */


    public VilleDAO(Connection connection)
    {
        super(connection);
    }


    @Override
    public <T> T getByID(String id)
    {
        Departement departement = new Departement("0", "");
        Ville ville = new Ville("0", "");
        ResultSet rs;
        try
        {

            String request = "SELECT * FROM V_LOCALITE WHERE VLE_NUM_INSEE=?";
            PreparedStatement stmt = connection.prepareStatement(request);
            stmt.setString(1, id);
            stmt.execute();
            rs = stmt.getResultSet();

            while (rs.next())
            {
                if (!rs.getString(5).equals(ville.getNumInsee()))
                {
                    ville = new Ville(rs.getString(5), rs.getString(6));
                    ville.setLatitude(rs.getFloat(7));
                    ville.setLongitude(rs.getFloat(8));
                    ville.setCodePostal(rs.getString(9));
                    Departement dptOfCity = new Departement(rs.getString(3), rs.getString(4));
                    dptOfCity.setRegion(new Region(rs.getString(1), rs.getString(2)));
                    ville.setDepartement(dptOfCity);

                }
            }

            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (T) ville;

    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        ArrayList<Ville> listVille = new ArrayList<>();
        Ville ville = new Ville("0", "");
        ResultSet rs;


        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_LOCALITE ORDER BY  VLE_CODE_POSTAL";
            rs = stmt.executeQuery(request);

            while (rs.next())
            {
                if (!rs.getString(5).equals(ville.getNumInsee()))
                {
                    ville = new Ville(rs.getString(5), rs.getString(6));
                    ville.setLatitude(rs.getFloat(7));
                    ville.setLongitude(rs.getFloat(8));
                    ville.setCodePostal(rs.getString(9));
                    Departement departement = new Departement(rs.getString(3), rs.getString(4));
                    departement.setRegion(new Region(rs.getString(1), rs.getString(2)));
                    ville.setDepartement(departement);
                    listVille.add(ville);
                }
            }
            rs.close();

        }
        catch (SQLException e)
        {

            e.printStackTrace();
        }


        return (ArrayList<T>) listVille;


    }


    public <T> ArrayList<T> getAllByRegion(String codeRegion)
    {


        {
            ArrayList<Ville> listVille = new ArrayList<>();
            Ville ville = new Ville("0", "");
            ResultSet rs;


            try
            {
                Statement stmt = connection.createStatement();
                String request = "SELECT * FROM V_LOCALITE WHERE RGN_CODE = " + codeRegion + "  ORDER BY  VLE_CODE_POSTAL ";
                rs = stmt.executeQuery(request);

                while (rs.next())
                {
                    if (!rs.getString(5).equals(ville.getNumInsee()))
                    {
                        ville = new Ville(rs.getString(5), rs.getString(6));
                        ville.setLatitude(rs.getFloat(7));
                        ville.setLongitude(rs.getFloat(8));
                        ville.setCodePostal(rs.getString(9));
                        Departement departement = new Departement(rs.getString(3), rs.getString(4));
                        departement.setRegion(new Region(rs.getString(1), rs.getString(2)));
                        ville.setDepartement(departement);
                        listVille.add(ville);
                    }
                }
                rs.close();

            }
            catch (SQLException e)
            {

                e.printStackTrace();
            }


            return (ArrayList<T>) listVille;


        }


    }


    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
     return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }

    @Override
    public <T> T getByID(int id)
    {
        return null;
    }

}
