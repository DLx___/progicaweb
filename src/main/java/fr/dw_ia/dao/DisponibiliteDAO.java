package fr.dw_ia.dao;

import fr.dw_ia.job.Disponibilite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DisponibiliteDAO extends DAO
{
    /**
     *  @author IOOSSEN AURELIEN created on 30/08/2019
     *
     */



    public DisponibiliteDAO(Connection connection)
    {
        super(connection);
    }


    public ArrayList<Disponibilite> getAllByPersonneContact(int idPersonnecontact){

        ArrayList<Disponibilite> listDisponibilitePersonne = new ArrayList<>();

        ResultSet rs;

        String request =" SELECT * FROM T_DISPONIBILITE" +
                " INNER JOIN T_ETRE_DISPONIBLE TED on T_DISPONIBILITE.DPT_ID = TED.EDP_DSP_ID" +
                " INNER JOIN T_PERSONNE TP on TED.EDP_PRS_ID = TP.PRS_ID\n" +
                " WHERE PRS_ID=" + idPersonnecontact;

        try
        {

            PreparedStatement pstmt = connection.prepareStatement(request);
            pstmt.execute();
            rs=pstmt.getResultSet();

            while(rs.next()){

              Disponibilite disponibilite = new Disponibilite(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5));
              listDisponibilitePersonne.add(disponibilite);

            }
        rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    return listDisponibilitePersonne;

    }


    @Override
    public <T> T getByID(int id)
    {
        return null;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        return null;
    }

    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
        return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }
}
