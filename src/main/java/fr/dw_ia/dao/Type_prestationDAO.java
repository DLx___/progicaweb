package fr.dw_ia.dao;

import fr.dw_ia.job.Sous_type_Prestation;
import fr.dw_ia.job.Type_prestation;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *  @author IOOSSEN AURELIEN created on 28/08/2019
 *
 */

public class Type_prestationDAO extends DAO
{
    public Type_prestationDAO(Connection connection)
    {
        super(connection);
    }


    @Override
    public <T> T getByID(int id)
    {
        Type_prestation type_prestation = new Type_prestation(0, "");
        Sous_type_Prestation sstype_prestation = new Sous_type_Prestation(0, "");

        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_PRESTATION  WHERE TYPE_ID=" + id;
            ResultSet rs = stmt.executeQuery(request);
            while (rs.next()){

                if (rs.getInt(5)!=type_prestation.getId_type()){
                    type_prestation = new Type_prestation(rs.getInt(5),rs.getString(6)) ;


                }
                sstype_prestation = new Sous_type_Prestation(rs.getInt(3),rs.getString(4)) ;
                type_prestation.addSousType(sstype_prestation);

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return (T) type_prestation;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        Type_prestation type_prestation = new Type_prestation(0,"");
        Sous_type_Prestation sstype_prestation = new Sous_type_Prestation(0,"");
        ArrayList<Type_prestation> listeType = new ArrayList<>();

        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_PRESTATION  ORDER BY TYPE_ID" ;
            ResultSet rs = stmt.executeQuery(request);
            while (rs.next()){

                if (rs.getInt(5)!=type_prestation.getId_type()){
                    type_prestation = new Type_prestation(rs.getInt(5),rs.getString(6)) ;
                }

                sstype_prestation = new Sous_type_Prestation(rs.getInt(3),rs.getString(4)) ;
                type_prestation.addSousType(sstype_prestation);
                listeType.add(type_prestation) ;

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return (ArrayList<T>) listeType;


    }

    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
        return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }


}
