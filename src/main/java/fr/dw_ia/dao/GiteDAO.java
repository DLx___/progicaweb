package fr.dw_ia.dao;

import fr.dw_ia.job.Gite;
import fr.dw_ia.job.Prestation;
import fr.dw_ia.job.Prestation_Gite;
import fr.dw_ia.service.GitesSearch;

import java.sql.*;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * @author IOOSSEN AURELIEN & WOJTOWICZ DENIS created on 28/08/2019
 */


public class GiteDAO extends DAO
{
    private Gite gite;

    public GiteDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        Gite gite = new Gite(0, "");
        try
        {
            String request = "SELECT * FROM T_GITE where GITE_ID =   " + id;
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                gite = new Gite(rs.getInt(1), rs.getString(2));
                gite.setSurfaceHabitable(rs.getInt(3));
                gite.setNmbCouchage(rs.getString(4));
                gite.setTarif(rs.getString(6));
                gite.setComplementGeo(rs.getString(7));
                gite.setComplementAdresse(rs.getString(8));
                gite.setNumero(rs.getString(9));
                gite.setRue(rs.getString(10));
                gite.setLieuDit(rs.getString(11));
                gite.setContact(DAOFactory.getPersonneDAO().getByID(rs.getInt(13)));
                gite.setProprietaire(DAOFactory.getPersonneDAO().getByID(rs.getInt(14)));

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (T) gite;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        ArrayList listGite = new ArrayList();
        Gite gite = new Gite(0, "");
        try
        {
            String request = "SELECT * FROM T_GITE ";
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                gite = new Gite(rs.getInt(1), rs.getString(2));

                gite.setSurfaceHabitable(rs.getInt(3));
                gite.setNmbCouchage(rs.getString(4));
                gite.setNmbChambre(rs.getString(5));
                gite.setTarif(rs.getString(6));
                gite.setComplementGeo(rs.getString(7));
                gite.setComplementAdresse(rs.getString(8));
                gite.setNumero(rs.getString(9));
                gite.setRue(rs.getString(10));
                gite.setLieuDit(rs.getString(11));
                gite.setContact(DAOFactory.getPersonneDAO().getByID(rs.getInt(13)));
                gite.setProprietaire(DAOFactory.getPersonneDAO().getByID(rs.getInt(14)));
                gite.setVille(DAOFactory.getCityDAO().getByID(rs.getString(12)));
                gite.setListePrestation(generatePrestationList(gite));
                listGite.add(gite);

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (ArrayList<T>) listGite;
    }

    public ArrayList<Gite> getFilteredGites(GitesSearch gitesSearch)
    {
        ArrayList<Gite> listFilteredGite = new ArrayList<>();
        Gite gite = new Gite(0, "");
        ResultSet rs;
        try
        {
            /*SQL SERVER*/
            String procedureStocked = "{CALL DBO.SP_GITES_QBE(?,?,?,?,?,?,?,?,?)}";
            /*POSTGRESQL*/     // String procedureStocked = "{CALL PUBLIC.SP_GITES_QBE(?,?,?,?,?,?,?,?,?)}";

            CallableStatement callableStatement = this.connection.prepareCall(procedureStocked);


            if (gitesSearch.getLibelle() == null)
            {
                callableStatement.setNull(1, Types.VARCHAR);
            }
            else
            {
                callableStatement.setString(1, gitesSearch.getLibelle());
            }

            if (gitesSearch.getRegion() == null || gitesSearch.getRegion().getRGN_code().equals(""))
            {
                callableStatement.setNull(2, Types.VARCHAR);
            }
            else
            {
                callableStatement.setString(2, gitesSearch.getRegion().getRGN_code());
            }

            if (gitesSearch.getDepartment() == null || gitesSearch.getDepartment().getNumeroDepartement().equals(""))
            {
                callableStatement.setNull(3, Types.VARCHAR);
            }
            else
            {
                callableStatement.setString(3, gitesSearch.getDepartment().getNumeroDepartement());
            }

            if (gitesSearch.getCity() == null || gitesSearch.getCity().getNumInsee().equals(""))
            {
                callableStatement.setNull(4, Types.VARCHAR);
            }
            else
            {
                callableStatement.setString(4, gitesSearch.getCity().getNomVille());
            }


            if (gitesSearch.getListBenefit().size() == 0)
            {
                callableStatement.setNull(5, Types.VARCHAR);
            }
            else
            {

                callableStatement.setString(5, convertListBenefitsToString(gitesSearch.getListBenefit()));
            }

            if (gitesSearch.getBedroomNumber() == 0)
            {
                callableStatement.setNull(6, Types.INTEGER);
            }
            else
            {
                callableStatement.setInt(6, gitesSearch.getBedroomNumber());
            }

            if (gitesSearch.getSearchRay() == 0)
            {
                callableStatement.setNull(7, Types.INTEGER);
            }
            else
            {
                callableStatement.setNull(2, Types.VARCHAR);
                callableStatement.setNull(3, Types.VARCHAR);
                callableStatement.setNull(4, Types.VARCHAR);

                callableStatement.setInt(7, gitesSearch.getSearchRay());

            }
            if (gitesSearch.getCity() == null)
            {
                callableStatement.setNull(8, Types.REAL);
                callableStatement.setNull(9, Types.REAL);
            }
            else
            {
                callableStatement.setFloat(8, gitesSearch.getCity().getLatitude());
                callableStatement.setFloat(9, gitesSearch.getCity().getLongitude());

            }
            System.out.println(callableStatement);
            callableStatement.execute();


            rs = callableStatement.getResultSet();

            // SI UTILISATION DE SQL SERVER => PLUSIEURS RESULTSET
            if (callableStatement.getMoreResults())
            {
                rs = callableStatement.getResultSet();

            }

            int compteur = 0;
            while (rs.next())
            {
                if (gite.getIdGite() != rs.getInt(1))
                {
                    gite = new Gite(rs.getInt(1), rs.getString(2));
                    gite.setSurfaceHabitable(rs.getInt(3));
                    gite.setNmbCouchage(rs.getString(4));
                    gite.setNmbChambre(rs.getString(5));
                    gite.setTarif(rs.getString(6));
                    gite.setComplementGeo(rs.getString(7));
                    gite.setComplementAdresse(rs.getString(8));
                    gite.setNumero(rs.getString(9));
                    gite.setRue(rs.getString(10));
                    gite.setLieuDit(rs.getString(11));
                    gite.setContact(DAOFactory.getPersonneDAO().getByID(rs.getInt(13)));
                    gite.setProprietaire(DAOFactory.getPersonneDAO().getByID(rs.getInt(14)));
                    gite.setVille(DAOFactory.getCityDAO().getByID(rs.getString(12)));
                    gite.setListePrestation(generatePrestationList(gite));

                    listFilteredGite.add(gite);
                    compteur++;


                }

            }
            rs.close();
            System.out.println("APPEL DAO : " + compteur + " Gites générés");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return listFilteredGite;
    }

    private String convertListBenefitsToString(ArrayList<Prestation> listBenefit)
    {
        StringBuilder listIdBenefits = new StringBuilder();

        ListIterator itr = listBenefit.listIterator();
        while (itr.hasNext())
        {
            if (itr.hasPrevious())
            {
                listIdBenefits.append(",");
            }

            Prestation pst = (Prestation) itr.next();
            String id = String.valueOf(pst.getId_prestation());
            listIdBenefits.append(id);


        }

        System.out.println(listIdBenefits.toString());
        return listIdBenefits.toString();


    }


    /**
     * @param gite
     * @return List of benefits gites
     */

    public ArrayList<Prestation_Gite> generatePrestationList(Gite gite)
    {
        return DAOFactory.getPrestationDAO().getAllByGite(gite.getIdGite());

    }

    @Override
    public boolean insert(Object o)
    {
        Gite gite = (Gite) o;
        try
        {

            String requestInsertion = "INSERT INTO T_GITE(GITE_NOM, GITE_SURFACE, GITE_NMBRE_COUCHAGE, GITE_NMBRE_CHAMBRE, GITE_TARIF_PROPRIETAIRE," + " GITE_COMPLEMENT_ADRESSE, GITE_COMPLEMENT_GEOGRAPHIQUE, GITE_NUMERO, GITE_LIBELLE_RUE, GITE_LIEU_DIT, GITE_NUM_INSEE," + " GITE_ID_CONTACT, GITE_ID_PROPRIETAIRE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = connection.prepareStatement(requestInsertion);

            pstmt.setString(1, gite.getLibelleGite());
            pstmt.setInt(2, gite.getSurfaceHabitable());
            pstmt.setInt(3, gite.getNmbCouchage());
            pstmt.setInt(4, gite.getNmbChambre());
            pstmt.setBigDecimal(5, gite.getTarif());
            pstmt.setString(6, gite.getComplementAdresse());
            pstmt.setString(7, gite.getComplementGeo());
            pstmt.setString(8, gite.getNumero());
            pstmt.setString(9, gite.getRue());
            pstmt.setString(10, gite.getLieuDit());
            pstmt.setString(11, gite.getVille().getNumInsee());
            pstmt.setInt(12, gite.getContact().getId_personne());
            pstmt.setInt(13, gite.getProprietaire().getId_personne());
            pstmt.execute();

            int idGiteinserted = getIdGiteInserted();

            ArrayList<Prestation_Gite> listPrestationGite = gite.getListePrestation();
            for (Prestation_Gite pstGite : listPrestationGite)
            {
                String requestInsertPrestation = "INSERT INTO T_PROPOSER_PRESTATION(PRP_PRIX, PRP_GITE_ID, PRP_PRESTATION_ID) VALUES (?,?,?)";
                PreparedStatement pstmtPrestation = connection.prepareStatement(requestInsertPrestation);
                pstmtPrestation.setInt(1, pstGite.getPrix_prestation());
                pstmtPrestation.setInt(2, idGiteinserted);
                pstmtPrestation.setInt(3, pstGite.getPrestation().getId_prestation());
                pstmtPrestation.execute();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public int getIdGiteInserted() throws SQLException
    {
        int id = 0;
        String requestGetidGite = "SELECT TOP 1 GITE_ID FROM T_GITE ORDER BY GITE_ID DESC";
        PreparedStatement pstmtId = connection.prepareStatement(requestGetidGite);
        pstmtId.execute();
        ResultSet rs = pstmtId.getResultSet();
        while (rs.next())
        {
            id = rs.getInt(1);
        }
        return id;
    }

    @Override
    public boolean delete(Object o)
    {
        System.out.println("Entrez dans la methoed delete");
        Gite gite = (Gite) o;
        try
        {

            String request = "DELETE FROM T_GITE WHERE GITE_ID = ?";
            PreparedStatement pstmt = connection.prepareStatement(request);
            pstmt.setInt(1, gite.getIdGite());
            pstmt.execute();
            System.out.println("gite supprimé");

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Object o)
    {

        Gite gite = (Gite) o;

        System.out.println("Entrez dans la methoed UPDATE :"+o.toString());
        String request = "UPDATE T_GITE SET GITE_NOM = ?,GITE_SURFACE = ?,GITE_NMBRE_CHAMBRE=?,GITE_NMBRE_COUCHAGE=?," +
                "GITE_TARIF_PROPRIETAIRE=?,GITE_COMPLEMENT_ADRESSE=?,GITE_COMPLEMENT_GEOGRAPHIQUE=?," +
                "GITE_NUMERO=?,GITE_LIBELLE_RUE=?,GITE_LIEU_DIT=?,GITE_NUM_INSEE=?,GITE_ID_CONTACT=?,GITE_ID_PROPRIETAIRE=?" +
                " WHERE GITE_ID=?";

        PreparedStatement pstmt = null;
        try
        {
            pstmt = connection.prepareStatement(request);
            pstmt.setString(1, gite.getLibelleGite());
            pstmt.setInt(2, gite.getSurfaceHabitable());
            pstmt.setInt(3, gite.getNmbChambre());
            pstmt.setInt(4, gite.getNmbCouchage());
            pstmt.setBigDecimal(5, gite.getTarif());
            pstmt.setString(6, gite.getComplementAdresse());
            pstmt.setString(7, gite.getComplementGeo());
            pstmt.setString(8, gite.getNumero());
            pstmt.setString(9, gite.getRue());
            pstmt.setString(10, gite.getLieuDit());
            pstmt.setString(11, gite.getVille().getNumInsee());
            pstmt.setInt(12, gite.getContact().getId_personne());
            pstmt.setInt(13, gite.getProprietaire().getId_personne());
            pstmt.setInt(14, gite.getIdGite());

            pstmt.execute();
        }

        catch (SQLException e)
        {
            e.printStackTrace();
        }



        return false;
    }

}
