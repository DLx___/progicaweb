package fr.dw_ia.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PROGICAConnect
{

    /**
     * @author IOOSSEN AURELIEN created on 27/08/2019
     */
    private static Connection connection;

    private PROGICAConnect()
    {

        String dbURL="jdbc:sqlserver://STA7400562:1433;databaseName=PROGICA";
        String user="java_user";
        String pass="1234";

//        String user = "java_user";
//        String pass = "1234";
//        String dbURL = "jdbc:sqlserver://STA7400544:1433;database=PROGICA";

//        String dbURL="jdbc:sqlserver://DLX:1433;database=PROGICA";
//        String user="java_user";
//        String pass="1234";


        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(dbURL, user, pass);
        }
        catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Singleton qui retourne une instance de Connection - connection BDD
     *
     * @return Connection
     */
    public static Connection getInstance()
    {

        if (connection == null)
        {

            new PROGICAConnect();
        }

        return connection;

    }


}
