package fr.dw_ia.dao;

import java.sql.Connection;
import java.util.ArrayList;

public abstract class DAO
{

    /**
     * Classe abstraite utilisé pour héritage
     *
     * @author IOOSSEN AURELIEN created on 27/08/2019
     */


    protected Connection connection;

    DAO(Connection connection)
    {
        this.connection = connection;
    }

    public abstract <T> T getByID(int id);

    public abstract <T> T getByID(String id);

    public abstract <T> ArrayList<T> getAll();

    public abstract boolean insert(Object o);

    public abstract boolean delete(Object o);

    public abstract boolean update(Object o);


}
