package fr.dw_ia.job;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Gite
{

    /**
     * @author IOOSSEN AURELIEN & WOJTOWICZ DENIS created on 27/08/2019
     */

    private Integer idGite;
    private String libelleGite;
    private Integer surfaceHabitable;
    private Integer nmbCouchage;
    private Integer nmbChambre;
    private BigDecimal tarif;
    private String complementAdresse;
    private String complementGeo;
    private String numero;
    private String rue;
    private String lieuDit;
    private Ville ville;
    private Personne proprietaire;
    private Personne contact;
    private ArrayList<Prestation_Gite> listePrestation;
    private ArrayList<Periode> listePeriode;

    public Gite(Integer idGite, String libelleGite)
    {
        this.idGite = idGite;
        this.libelleGite = libelleGite;
    }

    public Gite()
    {

    }

    /****************************************Getter/Setter****************************************/

    public Integer getIdGite()
    {
        return idGite;
    }

    public String getIdGiteString()
    {
        String gite = String.valueOf(idGite);
        return gite;
    }

    public void setIdGite(Integer idGite)
    {
        this.idGite = idGite;
    }

    public String getLibelleGite()
    {
        return libelleGite;
    }

    public void setLibelleGite(String libelleGite)
    {
        this.libelleGite = libelleGite;
    }

    public Integer getSurfaceHabitable()
    {
        return surfaceHabitable;
    }

    public void setSurfaceHabitable(Integer surfaceHabitable)
    {
        this.surfaceHabitable = surfaceHabitable;
    }

    public Integer getNmbCouchage()
    {
        return nmbCouchage;
    }

    public void setNmbCouchage(Integer nmbCouchage)
    {
        this.nmbCouchage = nmbCouchage;
    }

    public void setTarif(BigDecimal tarif)
    {
        this.tarif = tarif;
    }

    public void setNmbCouchage(String nmbCouchage)
    {
        this.nmbCouchage = Integer.parseInt(nmbCouchage);
    }

    public Integer getNmbChambre()
    {
        return nmbChambre;
    }

    public void setNmbChambre(Integer nmbChambre)
    {
        this.nmbChambre = nmbChambre;
    }

    public void setNmbChambre(String nmbChambre)
    {
        this.nmbChambre = Integer.parseInt(nmbChambre);
    }

    public BigDecimal getTarif()
    {
        return tarif;
    }

    public void setTarif(String tarif)
    {
        this.tarif = BigDecimal.valueOf(Double.parseDouble(tarif));
    }

    public String getComplementAdresse()
    {
        return complementAdresse;
    }

    public void setComplementAdresse(String complementAdresse)
    {
        this.complementAdresse = complementAdresse;
    }

    public String getComplementGeo()
    {
        return complementGeo;
    }

    public void setComplementGeo(String complementGeo)
    {
        this.complementGeo = complementGeo;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public String getRue()
    {
        return rue;
    }

    public void setRue(String rue)
    {
        this.rue = rue;
    }

    public String getLieuDit()
    {
        return lieuDit;
    }

    public void setLieuDit(String lieuDit)
    {
        this.lieuDit = lieuDit;
    }

    public Ville getVille()
    {
        return ville;
    }

    public void setVille(Ville ville)
    {
        this.ville = ville;
    }

    public Personne getProprietaire()
    {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire)
    {
        this.proprietaire = proprietaire;
    }

    public Personne getContact()
    {
        return contact;
    }

    public void setContact(Personne contact)
    {
        this.contact = contact;
    }

    public ArrayList<Prestation_Gite> getListePrestation()
    {
        return listePrestation;
    }

    public void setListePrestation(ArrayList<Prestation_Gite> listePrestation)
    {
        this.listePrestation = listePrestation;
    }

    public ArrayList<Periode> getListePeriode()
    {
        return listePeriode;
    }

    public void setListePeriode(ArrayList<Periode> listePeriode)
    {
        this.listePeriode = listePeriode;
    }

//    public String getPetPrice()
//    {String petPrice = "refusé" ;
//        for (Prestation_Gite pst : getListePrestation())
//        {
//            if (pst.getPrestation().getId_prestation() == 13){
//                petPrice = pst.getPrix_prestation() + "€" ;
//
//            }
//        }
//        return petPrice ;
//    }


    @Override
    public String toString()
    {
        return "Gite{" + "idGite=" + idGite + ", libelleGite='" + libelleGite + '\'' + ", surfaceHabitable='" + surfaceHabitable + '\'' + ", nmbCouchage=" + nmbCouchage + ", nmbChambre=" + nmbChambre + ", tarif=" + tarif + ", complementAdresse='" + complementAdresse + '\'' + ", complementGeo='" + complementGeo + '\'' + ", numero='" + numero + '\'' + ", rue='" + rue + '\'' + ", lieuDit='" + lieuDit + '\'' + ", ville=" + ville + ", proprietaire=" + proprietaire + ", contact=" + contact + ", listePrestation=" + listePrestation + ", listePeriode=" + listePeriode + '}';
    }
}
