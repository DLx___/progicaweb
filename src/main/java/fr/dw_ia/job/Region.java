package fr.dw_ia.job;

import java.util.ArrayList;

/**
 * @author IOOSSEN AURELIEN created on 27/08/2019
 */

public class Region
{

    private String RGN_code;
    private String RGN_nom;
    private ArrayList<Departement> listDepartement;


    public Region(String RGN_code, String RGN_nom)
    {
        this.RGN_code = RGN_code;
        this.RGN_nom = RGN_nom;
        listDepartement = new ArrayList<>();

    }

    public Region()
    {
    }

    public String getRGN_code()
    {
        return RGN_code;
    }

    public void setRGN_code(String RGN_code)
    {
        this.RGN_code = RGN_code;
    }

    public String getRGN_nom()
    {
        return RGN_nom;
    }

    public void setRGN_nom(String RGN_nom)
    {
        this.RGN_nom = RGN_nom;
    }

    public ArrayList<Departement> getListDepartement()
    {
        return listDepartement;
    }

    public void setListDepartement(ArrayList<Departement> listDepartement)
    {
        this.listDepartement = listDepartement;
    }

        @Override
    public String toString()
    {
        return RGN_code;
    }


}
