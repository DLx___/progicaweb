package fr.dw_ia.job;

public class Personne
{

    /**
     *  @author IOOSSEN AURELIEN created on 27/08/2019 
     *
     */


    private int id_personne;
    private String nom_personne;
    private String num_tph;
    private String mail;

    public Personne(int id_personne, String nom_personne, String num_tph, String mail)
    {
        this.id_personne = id_personne;
        this.nom_personne = nom_personne;
        this.num_tph = num_tph;
        this.mail = mail;
    }

    public Personne()
    {
    }

    public int getId_personne()
    {
        return id_personne;
    }

    public void setId_personne(int id_personne)
    {
        this.id_personne = id_personne;
    }

    public String getNom_personne()
    {
        return nom_personne;
    }

    public void setNom_personne(String nom_personne)
    {
        this.nom_personne = nom_personne;
    }

    public String getNum_tph()
    {
        return num_tph;
    }

    public void setNum_tph(String num_tph)
    {
        this.num_tph = num_tph;
    }


    public String getMail()
    {
        return mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String toStringGestion()
    {
        return id_personne + " - " + nom_personne;
    }

    @Override
    public String toString()
    {
        return nom_personne;
    }
}
