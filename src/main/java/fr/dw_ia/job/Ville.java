package fr.dw_ia.job;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author IOOSSEN AURELIEN created on 27/08/2019
 */


public class Ville
{
    private String numInsee;
    private String nomVille;
    private String codePostal;
    private Departement departement;
    private ArrayList<Gite> listeGite;
    private float latitude;
    private float longitude;


    public Ville(String num_insee, String nom_ville)
    {
        this.numInsee = num_insee;
        this.nomVille = nom_ville;
    }

    public Ville()
    {
    }

    public String getNumInsee()
    {
        return numInsee;
    }

    public void setNumInsee(String numInsee)
    {
        this.numInsee = numInsee;
    }

    public String getNomVille()
    {
        return nomVille;
    }

    public void setNomVille(String nomVille)
    {
        this.nomVille = nomVille;
    }

    public float getLatitude()
    {
        return latitude;
    }

    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }

    public float getLongitude()
    {
        return longitude;
    }

    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }

    public String getCodePostal()
    {
        return codePostal;
    }

    public void setCodePostal(String codePostal)
    {
        this.codePostal = codePostal;
    }

    public Departement getDepartement()
    {
        return departement;
    }

    public void setDepartement(Departement departement)
    {
        this.departement = departement;
    }


    public ArrayList<Gite> getListeGite()
    {
        return listeGite;
    }

    public void setListeGite(ArrayList<Gite> listeGite)
    {
        this.listeGite = listeGite;
    }



    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Ville ville = (Ville) o;
        return Float.compare(ville.latitude, latitude) == 0 && Float.compare(ville.longitude,
                                                                             longitude) == 0 && Objects.equals(numInsee,
                                                                                                               ville.numInsee) && Objects.equals(
                nomVille, ville.nomVille) && Objects.equals(codePostal, ville.codePostal) && Objects.equals(departement,
                                                                                                            ville.departement) && Objects.equals(
                listeGite, ville.listeGite);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(numInsee, nomVille, codePostal, departement, listeGite, latitude, longitude);
    }

    @Override
    public String toString()
    {
        if (getNumInsee().equals(""))
        {
            return codePostal;
        }
        else return codePostal + " - " + nomVille;
    }

    public String toStringDetails()
    {
       return codePostal + " - " + nomVille + " - " + numInsee;
    }
}
