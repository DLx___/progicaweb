package fr.dw_ia.service;

import fr.dw_ia.dao.DAOFactory;
import fr.dw_ia.job.Personne;
import fr.dw_ia.job.Ville;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author IOOSSEN AURELIEN created on 22/10/2019
 */

@FacesConverter("VilleConverter")
public class VilleConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String inseeVille)
    {

        Ville ville= DAOFactory.getCityDAO().getByID(inseeVille);
        return ville;

    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o)
    {
        Ville ville = (Ville) o;
        System.out.println("Converter ville"+ville);
        return ville.getNumInsee();
    }


}
