package fr.dw_ia.service;

import fr.dw_ia.dao.DAOFactory;
import fr.dw_ia.job.*;
import fr.dw_ia.rest.RestClient;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.GenericType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author WOJTOWICZ DENIS AND IOOSSEN AURELIEN created on 28/08/2019
 */

@SessionScoped
@ManagedBean
public class GitesBean implements Serializable
{
    String actionWanted;
    @ManagedProperty(value = "#{restClient}")
    private RestClient restClient;
    private ArrayList<Region> regions;
    private ArrayList<Region> regionsFilter;
    private ArrayList<Departement> departments;
    private ArrayList<Departement> departmentsFilter;
    private ArrayList<Ville> cities;
    private ArrayList<Ville> citiesFilter;
    private ArrayList<Prestation> benefits;
    private ArrayList<Prestation> benefitsFilter;
    private ArrayList<Gite> gites;
    private ArrayList<Disponibilite> disponibilites;
    private List<Personne> listPersonne;
    private GitesSearch giteSearched;
    private Gite giteSelected;
    private int idLastGiteCreated;


    public GitesBean()
    {
        regions = new ArrayList<>();
        departments = new ArrayList<>(DAOFactory.getDepartmentDAO().getAll());
        cities = new ArrayList<>(DAOFactory.getCityDAO().getAll());
        benefits = new ArrayList<>(DAOFactory.getPrestationDAO().getAll());
        gites = new ArrayList<>();
        listPersonne = new ArrayList<>(DAOFactory.getPersonneDAO().getAll());
        giteSelected = new Gite();
        actionWanted = "Création";

    }

    @PostConstruct
    private void initialize()
    {
        regions.addAll(restClient.get("regions").readEntity(new GenericType<List<Region>>()
        {
        }));

        gites.addAll(restClient.get("gites").readEntity(new GenericType<List<Gite>>()
        {
        }));
    }

    public List<String> regionList()
    {
        ArrayList<String> list = new ArrayList<>();
        for (Region region : regions)
        {
            list.add(region.getRGN_nom());
            System.out.println(list.size());
        }

        return list;
    }

    public String getActionWanted()
    {
        return actionWanted;
    }

    public void setActionWanted(String actionWanted)
    {
        this.actionWanted = actionWanted;
    }

    public List<String> personListString()
    {
        ArrayList<String> list = new ArrayList<>();
        for (Personne personne : listPersonne)
        {
            list.add(personne.getNom_personne());
        }

        return list;
    }


    public RestClient getRestClient()
    {
        return restClient;
    }

    public void setRestClient(RestClient restClient)
    {
        this.restClient = restClient;
    }

    public ArrayList<Region> getRegions()
    {
        return regions;
    }

    public void setRegions(ArrayList<Region> regions)
    {
        this.regions = regions;
    }

    public ArrayList<Region> getRegionsFilter()
    {
        return regionsFilter;
    }

    public void setRegionsFilter(ArrayList<Region> regionsFilter)
    {
        this.regionsFilter = regionsFilter;
    }

    public ArrayList<Departement> getDepartments()
    {
        return departments;
    }

    public void setDepartments(ArrayList<Departement> departments)
    {
        this.departments = departments;
    }

    public ArrayList<Departement> getDepartmentsFilter()
    {
        return departmentsFilter;
    }

    public void setDepartmentsFilter(ArrayList<Departement> departmentsFilter)
    {
        this.departmentsFilter = departmentsFilter;
    }

    public ArrayList<Ville> getCities()
    {
        return cities;
    }

    public void setCities(ArrayList<Ville> cities)
    {
        this.cities = cities;
    }

    public ArrayList<Ville> getCitiesFilter()
    {
        return citiesFilter;
    }

    public void setCitiesFilter(ArrayList<Ville> citiesFilter)
    {
        this.citiesFilter = citiesFilter;
    }

    public ArrayList<Prestation> getBenefits()
    {
        return benefits;
    }

    public void setBenefits(ArrayList<Prestation> benefits)
    {
        this.benefits = benefits;
    }

    public ArrayList<Prestation> getBenefitsFilter()
    {
        return benefitsFilter;
    }

    public void setBenefitsFilter(ArrayList<Prestation> benefitsFilter)
    {
        this.benefitsFilter = benefitsFilter;
    }

    public ArrayList<Gite> getGites()
    {
        return gites;
    }

    public void setGites(ArrayList<Gite> gites)
    {
        this.gites = gites;
    }

    public ArrayList<Disponibilite> getDisponibilites()
    {
        return disponibilites;
    }

    public void setDisponibilites(ArrayList<Disponibilite> disponibilites)
    {
        this.disponibilites = disponibilites;
    }

    public List<Personne> getListPersonne()
    {
        return listPersonne;
    }

    public void setListPersonne(List<Personne> listPersonne)
    {
        this.listPersonne = listPersonne;
    }

    public void setListPersonne(ArrayList<Personne> listPersonne)
    {
        this.listPersonne = listPersonne;
    }

    public GitesSearch getGiteSearched()
    {
        return giteSearched;
    }

    public void setGiteSearched(GitesSearch giteSearched)
    {
        this.giteSearched = giteSearched;
    }

    public Gite getGiteSelected()
    {
        return giteSelected;
    }

    public void setGiteSelected(Gite giteSelected)
    {
        System.out.println("gite selected !" + giteSelected);
        this.giteSelected = giteSelected;
    }

    public void selectioneLeGiteSelected(Gite giteSelected)
    {
        this.giteSelected = giteSelected;
    }

    public int getIdLastGiteCreated()
    {
        return idLastGiteCreated;
    }

    public void setIdLastGiteCreated(int idLastGiteCreated)
    {
        this.idLastGiteCreated = idLastGiteCreated;
    }

    public String delete(Gite giteSelected)
    {
        System.out.println("Suppression du gite : id" + giteSelected.getIdGite() + " libelle :" + giteSelected.getLibelleGite());
        DAOFactory.getGiteDAO().delete(giteSelected);

        return "faces/location.xhtml";
    }

    public String createNewGite()
    {
        giteSelected = null;
        actionWanted = "Création";
        return "gestion";
    }


    public String editGite()
    {
        actionWanted = "Edition";

        return "gestion";
    }

    public String saveGite()
    {
        if (actionWanted.equals("Edition"))
        {
            System.out.println("SelectedGite avant save" + giteSelected);
            DAOFactory.getGiteDAO().update(giteSelected);
            actionWanted = "Création";

        }
        return "index";
    }
}
