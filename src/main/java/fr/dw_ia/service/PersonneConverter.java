package fr.dw_ia.service;

import fr.dw_ia.job.Personne;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author IOOSSEN AURELIEN created on 22/10/2019
 */

@FacesConverter("PersonneConverter")
public class PersonneConverter implements Converter
{
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String idPersonne)
    {
        Personne per = new Personne(0,"","","") ;
        GitesBean gitesBean = new GitesBean();

        for (Personne p : gitesBean.getListPersonne())
        {

            if (String.valueOf(p.getId_personne()).equals(idPersonne))
            {
                per = p;
            }
        }

        return per;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o)
    {
        System.out.println("Personne :"+o);
        Personne personne = (Personne) o;
        return String.valueOf(personne.getId_personne());


    }


}
