package fr.dw_ia.service;

import fr.dw_ia.job.Departement;
import fr.dw_ia.job.Prestation;
import fr.dw_ia.job.Region;
import fr.dw_ia.job.Ville;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author WOJTOWICZ DENIS AND IOOSSEN AURELIEN created on 30/08/2019
 */
public class GitesSearch
{
    private String libelle;
    private Region region;
    private Departement department;
    private Ville city;
    private ArrayList<Prestation> listBenefit;
    private int bedroomNumber;
    private int searchRay;

    public GitesSearch()
    {
        // empty
    }

    public int getSearchRay()
    {
        return searchRay;
    }

    public void setSearchRay(int searchRay)
    {
        this.searchRay = searchRay;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public Region getRegion()
    {
        return region;
    }

    public void setRegion(Region region)
    {
        this.region = region;
    }

    public Departement getDepartment()
    {
        return department;
    }

    public void setDepartment(Departement department)
    {
        this.department = department;
    }

    public Ville getCity()
    {
        return city;
    }

    public void setCity(Ville city)
    {
        this.city = city;
    }

    public ArrayList<Prestation> getListBenefit()
    {
        return listBenefit;
    }

    public void setListBenefit(ArrayList<Prestation> listBenefit)
    {
        this.listBenefit = listBenefit;
    }

    public int getBedroomNumber()
    {
        return bedroomNumber;
    }

    public void setBedroomNumber(int bedroomNumber)
    {
        this.bedroomNumber = bedroomNumber;
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        GitesSearch that = (GitesSearch) o;
        return bedroomNumber == that.bedroomNumber && searchRay == that.searchRay && Objects.equals(libelle, that.libelle) && Objects.equals(region, that.region) && Objects.equals(department, that.department) && Objects.equals(city, that.city) && Objects.equals(listBenefit, that.listBenefit);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(libelle, region, department, city, listBenefit, bedroomNumber, searchRay);
    }

    @Override
    public String toString()
    {
        return "GitesSearch{" + "libelle='" + libelle + '\'' + ", region=" + region + ", department=" + department + ", city=" + city + ", listBenefit=" + listBenefit + ", bedroomNumber=" + bedroomNumber + ", searchRay=" + searchRay + '}';
    }
}
